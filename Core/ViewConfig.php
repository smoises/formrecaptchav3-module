<?php

/**
* @author  Mario Lorenz, www.the-real-world.de
* @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

namespace TheRealWorld\FormReCaptchaV3Module\Core;

use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\ToolsPlugin\Traits\ViewConfigTrait;
use TheRealWorld\FormReCaptchaV3Module\Core\FormReCaptchaV3Helper;

class ViewConfig extends ViewConfig_parent
{
    use ViewConfigTrait;

    /**
     * Template variable getter. Check if the captcha should be integrated into current view
     *
     * @return boolean
     */
    public function integrateReCaptchaV3()
    {
        return FormReCaptchaV3Helper::isReCaptchaV3IntegratedInFrontendController(
            $this->getCurrentFrontendControllerShort()
        );
    }
}