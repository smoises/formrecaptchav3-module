<?php

/**
* @author  Mario Lorenz, www.the-real-world.de
* @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

namespace TheRealWorld\FormReCaptchaV3Module\Core;

use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\ToolsPlugin\Core\ToolsMonologLogger;
use TheRealWorld\ToolsPlugin\Core\ToolsApi;

/**
* central Configuration for FormReCaptchaV3
*/
class FormReCaptchaV3Helper
{
    /**
    * the formelements
    * @var array
    */
    protected static $_aFormElements = [
        'invadr',
        'pa',
        'editval',
        'lgn_usr'
    ];

    /**
    * the FrontendController with ReCaptchaV3 Checks
    * @var string
    */
    protected static $_aControllerWithReCaptchaV3 = [
        'UserComponent'            => 'User',
        'UserController'           => 'User',
        'InviteController'         => 'User',
        'RegisterController'       => 'User',
        'ContactController'        => 'Contact',
        'SuggestController'        => 'Details',
        'ArticleDetailsController' => 'Details',
        'PriceAlarmController'     => 'Details',
        'NewsletterController'     => 'Newsletter',
        'ForgotPasswordController' => 'Password'
    ];

    /**
    * get the FrontendController with ReCaptchaV3 Checks
    *
    * @return array
    */
    public static function getFrontendControllerWithReCaptchaV3()
    {
        return self::$_aControllerWithReCaptchaV3;
    }

    /**
    * get the FrontendController with ReCaptchaV3 Checks
    *
    * @return bool
    */
    public static function isReCaptchaV3IntegratedInFrontendController($sFrontendController = '')
    {
        $bResult = false;
        if ($sOption = self::$_aControllerWithReCaptchaV3[$sFrontendController]) {
            $bResult = (bool)Registry::getConfig()->getConfigParam('bTRWFormReCaptchaV3Integration' . $sOption);
        }
        return $bResult;
    }

    /**
    * check Google ReCaptchaV3
    *
    * @param string $sFrontendController - FrontendController
    *
    * @return boolean
    */
    public static function checkReCaptchaV3($sFrontendController)
    {
        $bResult = false;
        $oConfig = Registry::getConfig();

        if (
            $oConfig->getConfigParam('bTRWFormReCaptchaV3UseReCaptchaV3') &&
            self::isReCaptchaV3IntegratedInFrontendController($sFrontendController)
        ) {
            $bThrowExceptionOnError = $oConfig->getConfigParam('bTRWFormReCaptchaV3ThrowExceptionOnError');
            $bThrowMessageOnError = $oConfig->getConfigParam('bTRWFormReCaptchaV3ThrowMessageOnError');

            // check if request not valid ReCaptchaV3
            $bNotValidReCaptchaV3 = false;

            $sRecaptchaResult = ToolsApi::executeExternApiFileGetContents(
                'https://www.google.com/recaptcha/api/siteverify',
                [
                    'secret'   => $oConfig->getConfigParam('sTRWFormReCaptchaV3SecretKey'),
                    'response' => $oConfig->getRequestParameter('recaptcha_response')
                ]
            );
            $oRecaptchaResult = json_decode($sRecaptchaResult);

            // Take action based on the score returned
            // According to the documentation,
            // 1.0 is very likely a good interaction,
            // 0.0 is very likely a bot'.
            // For simplicity, I'm accepting all submissions
            // from any user with a score of 0.5 or above.
            if ($oRecaptchaResult->score <= floatval($oConfig->getConfigParam('sTRWFormReCaptchaV3Score'))) {
                $bNotValidReCaptchaV3 = true;
            }

            // collect Controller
            $aRequestData = [
                'controller' => $sFrontendController,
            ];

            // collect IP
            if ($oConfig->getConfigParam('bTRWFormReCaptchaV3CollectIP')) {
                $aRequestData['clientIP'] = $_SERVER['REMOTE_ADDR'];
            }

            // collect full request
            foreach (self::$_aFormElements as $sFormElements) {
                if ($mRequestParameter = $oConfig->getRequestParameter($sFormElements)) {
                    $aRequestData = array_merge(
                        $aRequestData,
                        $mRequestParameter
                    );
                }
            }

            // Error Handling
            if (($bThrowExceptionOnError || $bThrowMessageOnError) && $bNotValidReCaptchaV3) {
                $bResult = true;

                if ($bThrowMessageOnError) {
                    Registry::getUtilsView()->addErrorToDisplay('ERROR_MESSAGE_RECAPTCHAV3_MESSAGE');
                }
                if ($bThrowExceptionOnError) {
                    self::_writeToLog(
                        'ERROR_MESSAGE_RECAPTCHAV3_EXCEPTION',
                        $aRequestData
                    );
                }
            }

            // Log Handling
            if (self::_shouldWriteLogForFrontendController($sFrontendController)) {
                self::_writeToLog(
                    'INFO_MESSAGE_RECAPTCHAV3_EXCEPTION',
                    $aRequestData,
                    'info'
                );
            }
        }
        return $bResult;
    }

    /**
    * should write Log For FrontendController?
    *
    * @return bool
    */
    private static function _shouldWriteLogForFrontendController($sFrontendController = '')
    {
        $bResult = false;
        if ($sOption = self::$_aControllerWithReCaptchaV3[$sFrontendController]) {
            $bResult = (bool)Registry::getConfig()->getConfigParam('bTRWFormReCaptchaV3WriteLogFor' . $sOption);
        }
        return $bResult;
    }

    /**
    * write a Exeption Message to the log
    *
    * @param string $sMessage (oLang Variable)
    * @param mixed  $mData (Data for logging), would be converted to string
    * @param string $sLogLevel (e.g. error, info, warning ... see Monolog\Logger
    *
    * @return null
    */
    private static function _writeToLog($sMessage, $mData, $sLogLevel = 'error')
    {
        $oConfig = Registry::getConfig();

        $sData = '';
        if (is_array($mData)) {
            // help: build array to string without print_r
            $sData = str_replace('=', ':', http_build_query($mData, null, ','));
        }

        $oEx = oxNew(\OxidEsales\Eshop\Core\Exception\InputException::class);
        $oEx->setMessage(sprintf(
            Registry::getLang()->translateString($sMessage),
            $sData
        ));

        $oLogger = ToolsMonologLogger::getLogger('trwformrecaptchav3');

        call_user_func([$oLogger, $sLogLevel], $oEx->getMessage());

        return null;
    }
}
