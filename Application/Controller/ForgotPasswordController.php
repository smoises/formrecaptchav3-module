<?php

/**
* @author  Mario Lorenz, www.the-real-world.de, Stefan Moises Proudcommerce
* @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

namespace TheRealWorld\FormReCaptchaV3Module\Application\Controller;

use TheRealWorld\FormReCaptchaV3Module\Core\FormReCaptchaV3Helper;

/**
* contact class
*
* @mixin \OxidEsales\Eshop\Application\Controller\ForgotPasswordController
*/
class ForgotPasswordController extends ForgotPasswordController_parent
{
    /**
    * @inerhitDoc
    *
    * @return bool
    */
    public function forgotPassword()
    {
        if (FormReCaptchaV3Helper::checkReCaptchaV3((new \ReflectionClass($this))->getShortName())) {
            return false;
        }
        return parent::forgotPassword();
    }
}
