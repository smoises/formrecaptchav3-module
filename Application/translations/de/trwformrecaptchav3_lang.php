<?php

/**
* @author  Mario Lorenz, www.the-real-world.de
* @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset'                                            => 'UTF-8',

    'ERROR_MESSAGE_RECAPTCHAV3_MESSAGE'                  => 'Das Formular wurde missbräuchlich genutzt und daher nicht abgesendet.',
    'ERROR_MESSAGE_RECAPTCHAV3_EXCEPTION'                => '!Formularmissbrauch! Daten: %s',
    'INFO_MESSAGE_RECAPTCHAV3_EXCEPTION'                 => '!Formularnutzung! Daten: %s'
];
