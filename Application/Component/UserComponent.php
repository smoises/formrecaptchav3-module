<?php

/**
* @author  Mario Lorenz, www.the-real-world.de
* @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

namespace TheRealWorld\FormReCaptchaV3Module\Application\Component;

use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\FormReCaptchaV3Module\Core\FormReCaptchaV3Helper;

/**
* User object manager.
*
* @mixin \OxidEsales\Eshop\Application\Component\UserComponent
*/
class UserComponent extends UserComponent_parent
{
    /**
    * @inerhitDoc
    *
    * @return  mixed redirection string or true if successful, false otherwise
    */
    public function createUser()
    {
        if (FormReCaptchaV3Helper::checkReCaptchaV3((new \ReflectionClass($this))->getShortName())) {
            return false;
        }
        return parent::createUser();
    }
}
