<?php

/**
* @author  Mario Lorenz, www.the-real-world.de
* @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset'                                                        => 'UTF-8',

    'SHOP_MODULE_GROUP_trwformrecaptchav3googleapi'                  => 'Google Recaptcha v3 API',
    'SHOP_MODULE_GROUP_trwformrecaptchav3control'                    => 'Controlling',
    'SHOP_MODULE_GROUP_trwformrecaptchav3integrations'               => 'Integrations',

    'SHOP_MODULE_bTRWFormReCaptchaV3UseReCaptchaV3'                  => 'use Google ReCaptcha v3 Protection',
    'SHOP_MODULE_bTRWFormReCaptchaV3ThrowMessageOnError'             => 'Show in the frontend an error message when the recaptchav3 was used.',
    'SHOP_MODULE_bTRWFormReCaptchaV3ThrowExceptionOnError'           => 'Create an error log entry if someone has fallen into the recaptchav3.',
    'HELP_SHOP_MODULE_bTRWFormReCaptchaV3ThrowExceptionOnError'      => 'The OXID log stores the complete form input.',
    'SHOP_MODULE_bTRWFormReCaptchaV3ThrowExceptionOnUse'             => 'Create an info log entry if someone has used the form.',
    'HELP_SHOP_MODULE_bTRWFormReCaptchaV3ThrowExceptionOnUse'        => 'The OXID log stores the complete form input. This option should only be used if the honey pot seems to be bypassed. Then you can see how it is being made to optimize the hidden recaptchav3 form fields.',
    'SHOP_MODULE_bTRWFormReCaptchaV3CollectIP'                       => 'Should the IP be logged?',
    'HELP_SHOP_MODULE_bTRWFormReCaptchaV3CollectIP'                  => 'Optional: Please discuss this option with your privacy officer. The logging of the IP in case of abuse can be correct, but not with the general logging of the form input.',
    'SHOP_MODULE_bTRWFormReCaptchaV3WriteLogForUser'                 => 'Generate "Use"-logs for register forms',
    'SHOP_MODULE_bTRWFormReCaptchaV3WriteLogForNewsletter'           => 'Generate "Use"-logs for newsletter form',
    'SHOP_MODULE_bTRWFormReCaptchaV3WriteLogForContact'              => 'Generate "Use"-logs for conctact form',
    'SHOP_MODULE_bTRWFormReCaptchaV3WriteLogForDetails'              => 'Generate "Use"-logs for detail page forms (pricealarm, suggest)',
    'SHOP_MODULE_bTRWFormReCaptchaV3WriteLogForPassword'             => 'Generate "Use"-logs forgot password form',
    'SHOP_MODULE_sTRWFormReCaptchaV3WebsiteKey'                      => 'Website Key',
    'HELP_SHOP_MODULE_sTRWFormReCaptchaV3WebsiteKey'                 => 'available at: https://www.google.com/recaptcha/intro/v3.html',
    'SHOP_MODULE_sTRWFormReCaptchaV3SecretKey'                       => 'Secret Key',
    'HELP_SHOP_MODULE_sTRWFormReCaptchaV3SecretKey'                  => 'available at: https://www.google.com/recaptcha/intro/v3.html',
    'SHOP_MODULE_sTRWFormReCaptchaV3Score'                           => 'Spam Score',
    'HELP_SHOP_MODULE_sTRWFormReCaptchaV3Score'                      => 'Each form use is rated with a score. At what score should a form use be classified as spam? (0.0 = bot, 1.0 = very good interaction)',
    'SHOP_MODULE_sTRWFormReCaptchaV3Score_0.0'                       => '0.0',
    'SHOP_MODULE_sTRWFormReCaptchaV3Score_0.1'                       => '0.1',
    'SHOP_MODULE_sTRWFormReCaptchaV3Score_0.2'                       => '0.2',
    'SHOP_MODULE_sTRWFormReCaptchaV3Score_0.3'                       => '0.3',
    'SHOP_MODULE_sTRWFormReCaptchaV3Score_0.4'                       => '0.4',
    'SHOP_MODULE_sTRWFormReCaptchaV3Score_0.5'                       => '0.5',
    'SHOP_MODULE_sTRWFormReCaptchaV3Score_0.6'                       => '0.6',
    'SHOP_MODULE_sTRWFormReCaptchaV3Score_0.7'                       => '0.7',
    'SHOP_MODULE_sTRWFormReCaptchaV3Score_0.8'                       => '0.8',
    'SHOP_MODULE_sTRWFormReCaptchaV3Score_0.9'                       => '0.9',
    'SHOP_MODULE_sTRWFormReCaptchaV3Score_1.0'                       => '1.0',
    'SHOP_MODULE_bTRWFormReCaptchaV3IntegrationUser'                 => 'Add to register forms?',
    'SHOP_MODULE_bTRWFormReCaptchaV3IntegrationNewsletter'           => 'Add to newsletter form?',
    'SHOP_MODULE_bTRWFormReCaptchaV3IntegrationContact'              => 'Add to conctact form?',
    'SHOP_MODULE_bTRWFormReCaptchaV3IntegrationDetails'              => 'Add to detail page forms (pricealarm, suggest)?',
    'SHOP_MODULE_bTRWFormReCaptchaV3IntegrationPassword'             => 'Add to forgot password form?'
];
