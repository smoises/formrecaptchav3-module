<?php

/**
* @author  Mario Lorenz, www.the-real-world.de
* @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset'                                                        => 'UTF-8',

    'SHOP_MODULE_GROUP_trwformrecaptchav3googleapi'                  => 'Google Recaptcha v3 API',
    'SHOP_MODULE_GROUP_trwformrecaptchav3control'                    => 'Kontrolle',
    'SHOP_MODULE_GROUP_trwformrecaptchav3integrations'               => 'Integration',

    'SHOP_MODULE_bTRWFormReCaptchaV3UseReCaptchaV3'                  => 'Nutze Google ReCaptcha V3 Schutz',
    'SHOP_MODULE_bTRWFormReCaptchaV3ThrowMessageOnError'             => 'Zeige im Frontend eine Fehlermeldung, wenn der Google ReCaptcha V3 Schutz genutzt wurde.',
    'SHOP_MODULE_bTRWFormReCaptchaV3ThrowExceptionOnError'           => 'Erzeuge einen Error-Log-Eintrag wenn jemand den Google ReCaptcha V3 Schutz nicht besteht.',
    'HELP_SHOP_MODULE_bTRWFormReCaptchaV3ThrowExceptionOnError'      => 'Im OXID-Log wird die komplette Formulareingabe gespeichert.',
    'SHOP_MODULE_bTRWFormReCaptchaV3ThrowExceptionOnUse'             => 'Erzeuge einen Info-Log-Eintrag wenn jemand das Formular genutzt hat.',
    'HELP_SHOP_MODULE_bTRWFormReCaptchaV3ThrowExceptionOnUse'        => 'Im OXID-Log wird die komplette Formulareingabe gespeichert. Diese Option sollte nur genutzt werden, wenn scheinbar der Honigtopf umgangen wird. Dann kann man schauen, wie gespamt wird um die versteckten Formularfelder des Honigtopfs noch zu optimieren.',
    'SHOP_MODULE_bTRWFormReCaptchaV3CollectIP'                       => 'Soll die IP geloggt werden?',
    'HELP_SHOP_MODULE_bTRWFormReCaptchaV3CollectIP'                  => 'Optional: Bitte besprechen Sie diese Option mit Ihrem Datenschutzbeauftragten. Das loggen der IP im Missbrauchsfall kann korrekt sein, jedoch nicht beim generellen loggen der Formulareingaben.',
    'SHOP_MODULE_bTRWFormReCaptchaV3WriteLogForUser'                 => '"Nutzungs"-Logs erzeugen für Registrierungsformulare',
    'SHOP_MODULE_bTRWFormReCaptchaV3WriteLogForNewsletter'           => '"Nutzungs"-Logs erzeugen für Newsletter-Formular',
    'SHOP_MODULE_bTRWFormReCaptchaV3WriteLogForContact'              => '"Nutzungs"-Logs erzeugen für Kontakt-Formular',
    'SHOP_MODULE_bTRWFormReCaptchaV3WriteLogForDetails'              => '"Nutzungs"-Logs erzeugen für Detailseite (Preisalarm, Empfehlung)',
    'SHOP_MODULE_bTRWFormReCaptchaV3WriteLogForPassword'             => '"Nutzungs"-Logs erzeugen für Passwort-Vergessen Formular',
    'SHOP_MODULE_DescriptionUserComponent'                           => 'Registrierung',
    'SHOP_MODULE_DescriptionInviteController'                        => 'Einladung',
    'SHOP_MODULE_DescriptionContactController'                       => 'Kontakt',
    'SHOP_MODULE_DescriptionSuggestController'                       => 'Empfehlung',
    'SHOP_MODULE_DescriptionArticleDetailsController'                => 'Preisalarm im Artikel Detail',
    'SHOP_MODULE_DescriptionPriceAlarmController'                    => 'Preisalarm',
    'SHOP_MODULE_DescriptionNewsletterController'                    => 'Newsletter Anmeldung',
    'SHOP_MODULE_DescriptionForgotPasswordController'                => 'Passwort vergessen',
    'SHOP_MODULE_sTRWFormReCaptchaV3WebsiteKey'                      => 'Webseite Schlüssel',
    'HELP_SHOP_MODULE_sTRWFormReCaptchaV3WebsiteKey'                 => 'erhältlich unter: https://www.google.com/recaptcha/intro/v3.html',
    'SHOP_MODULE_sTRWFormReCaptchaV3SecretKey'                       => 'Geheimer Schlüssel',
    'HELP_SHOP_MODULE_sTRWFormReCaptchaV3SecretKey'                  => 'erhältlich unter: https://www.google.com/recaptcha/intro/v3.html',
    'SHOP_MODULE_sTRWFormReCaptchaV3Score'                           => 'Spam Score',
    'HELP_SHOP_MODULE_sTRWFormReCaptchaV3Score'                      => 'Jede Formularnutzung wird mit einem Score bewertet. Ab welchem Score soll eine Formularnutzung als Spam eingestuft werden? (0.0 = Bot, 1.0 = sehr gute Interaktion)',
    'SHOP_MODULE_sTRWFormReCaptchaV3Score_0.0'                       => '0.0',
    'SHOP_MODULE_sTRWFormReCaptchaV3Score_0.1'                       => '0.1',
    'SHOP_MODULE_sTRWFormReCaptchaV3Score_0.2'                       => '0.2',
    'SHOP_MODULE_sTRWFormReCaptchaV3Score_0.3'                       => '0.3',
    'SHOP_MODULE_sTRWFormReCaptchaV3Score_0.4'                       => '0.4',
    'SHOP_MODULE_sTRWFormReCaptchaV3Score_0.5'                       => '0.5',
    'SHOP_MODULE_sTRWFormReCaptchaV3Score_0.6'                       => '0.6',
    'SHOP_MODULE_sTRWFormReCaptchaV3Score_0.7'                       => '0.7',
    'SHOP_MODULE_sTRWFormReCaptchaV3Score_0.8'                       => '0.8',
    'SHOP_MODULE_sTRWFormReCaptchaV3Score_0.9'                       => '0.9',
    'SHOP_MODULE_sTRWFormReCaptchaV3Score_1.0'                       => '1.0',
    'SHOP_MODULE_bTRWFormReCaptchaV3IntegrationUser'                 => 'In den Registrierungsformularen einbinden?',
    'SHOP_MODULE_bTRWFormReCaptchaV3IntegrationNewsletter'           => 'Im Newsletter-Formular einbinden?',
    'SHOP_MODULE_bTRWFormReCaptchaV3IntegrationContact'              => 'Im Kontakt-Formular einbinden?',
    'SHOP_MODULE_bTRWFormReCaptchaV3IntegrationDetails'              => 'Auf Detailseite (Preisalarm, Empfehlung) einbinden?',
    'SHOP_MODULE_bTRWFormReCaptchaV3IntegrationPassword'             => 'Im Passwort-Vergessen Formular einbinden?'
];
