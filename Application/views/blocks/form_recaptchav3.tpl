[{$smarty.block.parent}]
[{assign var="oConfig" value=$oViewConf->getConfig()}]
[{if $oViewConf|method_exists:'integrateReCaptchaV3' && $oViewConf->integrateReCaptchaV3()}]
    [{assign var="sReCaptchaV3WebsiteKey" value=$oConfig->getConfigParam('sTRWFormReCaptchaV3WebsiteKey')}]
    [{oxscript include="https://www.google.com/recaptcha/api.js?render="|cat:$sReCaptchaV3WebsiteKey priority=5}]
    [{capture assign="sGoogleRecaptchaV3"}]
        [{strip}]
            grecaptcha.ready(function () {
                grecaptcha.execute('[{$sReCaptchaV3WebsiteKey}]', { action: '[{$oViewConf->getTopActiveClassName()}]' }).then(function (token) {
                    var recaptchaResponse = document.getElementById('recaptchaResponse');
                    recaptchaResponse.value = token;
                });
                setInterval(function(){
                    grecaptcha.execute('[{$sReCaptchaV3WebsiteKey}]', {action: '[{$oViewConf->getTopActiveClassName()}]'}).then(function(token) {
                    [{if $oConfig->getConfigParam('bTRWFormReCaptchaV3ShowInfoLink')}]
                        console.log( 'refreshed recaptcha token:', token );
                    [{/if}]
                    recaptchaResponse.value = token;
                    });
                }, 60000);
            });
        [{/strip}]
    [{/capture}]
    [{oxscript add=$sGoogleRecaptchaV3}]
    <input type="hidden" name="recaptcha_response" id="recaptchaResponse" />
[{/if}]